package mundo_bibtex_file;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.StringTokenizer;

import javax.security.auth.callback.LanguageCallback;

public class bibtex_file {
	/**
	 * Representacion del archivo
	 */
	private File archivo;
	/**
	 * Lector del archivo
	 */
	private Scanner sc;
	/**
	 * Arreglo con los libros
	 */

	private ArrayList<String> arrayDatos;

	private ArrayList<Integer> arrayErroresLinea;

	private int totalErrores;
	
	private BufferedReader br;
	
	private BufferedReader br2;

	private StringTokenizer st;

	private FileReader fr;
	
	private ArrayList<String> faltantes;

	/**
	 * 
	 * @param pRuta
	 * @throws IOException
	 */
	public bibtex_file(String pRuta) throws IOException {
		arrayDatos = new ArrayList<String>();
		totalErrores=0;
		faltantes = new ArrayList<String>();
		arrayErroresLinea = new ArrayList<Integer>();
		archivo = new File(pRuta);
		fr = new FileReader(archivo);
		br = new BufferedReader(fr);
		leer();
//		br2 = new BufferedReader(fr);
//		sc = new Scanner(archivo);
		
		contarErrores();
		erroresDatos();
		
	}

	private void leer() throws IOException {

		ArrayList<String> arreglo = new ArrayList<String>();
		int x = 0;

		String act = br.readLine();
		

		while (act != null) {
			arreglo.add(act);
			act = br.readLine();
		}

		String[] jeje = arreglo.toString().split("@");

		for (int i = 1; i < jeje.length; i++) {
			String act2 = jeje[i];
			arrayDatos.add(act2);
		}

	}

	private void erroresDatos() {
		for(int i = 0 ; i< arrayDatos.size();i++)
		{
			String act = arrayDatos.get(i).toLowerCase();
			String[] info = act.split(",");
			String[] id = info[0].split("\\{");
			ArrayList<String> errores= new ArrayList<String>();
			int contador= 0; 
			String ID =null;
			try {
				ID= id[1];
			}catch (Exception e)
			{
				ID="Falta id en la entrada: " + i;
			}
			if (act.contains("article{"))

			{
				errores=new ArrayList<String>();
				contador=0;

				if (!act.toLowerCase().contains("author"))
				{
					errores.add("Author");
					contador++;
				}
				if (!act.toLowerCase().contains("title"))
				{
					errores.add("Title");
					contador++;
				}
				if (!act.toLowerCase().contains("journal")) {
					contador++;
					errores.add("Journal");
				}
				if (!act.toLowerCase().contains("year")) {
					contador++;
					errores.add("Year");
				}
				if (!act.toLowerCase().contains("Volume")) {
					contador++;
					errores.add("Volume");
				}
				if (contador<0)
				{
					errores.add(0, ID);
					faltantes.add(errores.toString());
				}
			}
			// book
			if (act.contains("book{"))

			{
				errores=new ArrayList<String>();
				contador=0;

				if (!act.toLowerCase().contains("author"))
				{
					if(!act.toLowerCase().contains("editor"))
					{
						errores.add("Author/Editor");
						contador++;
					}

				}
				if (!act.toLowerCase().contains("title"))
				{
					errores.add("Title");
					contador++;
				}
				if (!act.toLowerCase().contains("publisher")) {
					contador++;
					errores.add("Publiser");
				}
				if (!act.toLowerCase().contains("year")) {
					contador++;
					errores.add("Year");
				}

				if (contador!=0)
				{
					errores.add(0, ID);
					faltantes.add(errores.toString());
				}
			}
			// booklet
			if (act.contains("booklet{"))

			{
				errores=new ArrayList<String>();
				contador=0;


				if (!act.toLowerCase().contains("title"))
				{
					errores.add("Title");
					contador++;
				}
				if (contador!=0)
				{
					errores.add(0, ID);
					faltantes.add(errores.toString());
				}
			}

			//Conference

			if (act.contains("conference{"))

			{
				errores=new ArrayList<String>();
				contador=0;

				if (!act.contains("author"))
				{
					errores.add("Author");
					contador++;
				}
				if (!act.contains("title"))
				{
					errores.add("Title");
					contador++;
				}
				if (!act.contains("booktitle")) {
					contador++;
					errores.add("Booktitle");
				}
				if (!act.contains("year")) {
					contador++;
					errores.add("Year");
				}

				if (contador!=0)
				{
					errores.add(0, ID);
					faltantes.add(errores.toString());
				}
			}

			//inbook
			if (act.contains("inbook{"))

			{
				errores=new ArrayList<String>();
				contador=0;

				if (!act.contains("author") || !act.contains("Editor"))
				{
					errores.add("Author/Editor");
					contador++;
				}
				if (!act.contains("title"))
				{
					errores.add("Title");
					contador++;
				}
				if (!act.contains("chapter")|| !act.contains("pages")) {
					contador++;
					errores.add("Chapter/Pages");
				}
				if (!act.contains("year")) {
					contador++;
					errores.add("Year");
				}
				if (!act.contains("publisher")) {
					contador++;
					errores.add("Publisher");
				}
				if (contador!=0)
				{
					errores.add(0, ID);
					faltantes.add(errores.toString());
				}
			}
			//inCollection
			if (act.contains("incollection{"))

			{
				errores=new ArrayList<String>();
				contador=0;

				if (!act.contains("author"))
				{
					errores.add("Author");
					contador++;
				}
				if (!act.contains("title"))
				{
					errores.add("Title");
					contador++;
				}
				if (!act.contains("booktitle")) {
					contador++;
					errores.add("Booktitle");
				}
				if (!act.contains("year")) {
					contador++;
					errores.add("Year");
				}
				if (!act.contains("publisher")) {
					contador++;
					errores.add("Publisher");
				}
				if (contador!=0)
				{
					errores.add(0, ID);
					faltantes.add(errores.toString());
				}
			}
			//Inproceeding

			if (act.contains("inproceeding{"))

			{
				errores=new ArrayList<String>();
				contador=0;

				if (!act.contains("author"))
				{
					errores.add("Author");
					contador++;
				}
				if (!act.contains("title"))
				{
					errores.add("Title");
					contador++;
				}
				if (!act.contains("booktitle")) {
					contador++;
					errores.add("Booktitle");
				} 	
				if (!act.contains("year")) {
					contador++;
					errores.add("Year");
				}
				if (contador!=0)
				{
					errores.add(0, ID);
					faltantes.add(errores.toString());
				}
			}
			// manual

			if (act.contains("manual{"))

			{
				errores=new ArrayList<String>();
				contador=0;


				if (!act.contains("title"))
				{
					errores.add("Title");
					contador++;
				}
				if (contador!=0)
				{
					errores.add(0, ID);
					faltantes.add(errores.toString());
				}
			}
			//mastersthesis
			if (act.contains("mastersthesis{"))

			{
				errores=new ArrayList<String>();
				contador=0;

				if (!act.contains("author"))
				{
					errores.add("Author");
					contador++;
				}
				if (!act.contains("title"))
				{
					errores.add("Title");
					contador++;
				}
				if (!act.contains("school")) {
					contador++;
					errores.add("School");
				}
				if (!act.contains("year")) {
					contador++;
					errores.add("Year");
				}

				if (contador!=0)
				{
					errores.add(0, ID);
					faltantes.add(errores.toString());
				}
			}

			//phdThesis
			if (act.contains("phdthesis{"))

			{
				errores=new ArrayList<String>();
				contador=0;

				if (!act.contains("author"))
				{
					errores.add("Author");
					contador++;
				}
				if (!act.contains("title"))
				{
					errores.add("Title");
					contador++;
				}
				if (!act.contains("school")) {
					contador++;
					errores.add("School");
				}
				if (!act.contains("year")) {
					contador++;
					errores.add("Year");
				}
				if (contador!=0)
				{
					errores.add(0, ID);
					faltantes.add(errores.toString());
				}
			}
			//proceedings
			if (act.contains("proceedings{"))

			{
				errores=new ArrayList<String>();
				contador=0;


				if (!act.contains("title"))
				{
					errores.add("Title");
					contador++;
				}

				if (!act.contains("year")) {
					contador++;
					errores.add("Year");
				}
				if (contador!=0)
				{
					errores.add(0, ID);
					faltantes.add(errores.toString());
				}
			}
			//TechReport
			if (act.contains("techreport{"))
				
			{
				errores=new ArrayList<String>();
				contador=0;

				if (!act.contains("author"))
				{
					errores.add("Author");
					contador++;
				}
				if (!act.contains("title"))
				{
					errores.add("Title");
					contador++;
				}
				if (!act.contains("institution")) {
					contador++;
					errores.add("Journal");
				}
				if (!act.contains("year")) {
					contador++;
					errores.add("Year");
				}
				if (contador!=0)
				{
					errores.add(0, ID);
					faltantes.add(errores.toString());
				}
			}
			//unpublished
			if (act.contains("unpublished{"))

			{
				errores=new ArrayList<String>();
				contador=0;

				if (!act.contains("author"))
				{
					errores.add("Author");
					contador++;
				}
				if (!act.contains("title"))
				{
					errores.add("Title");
					contador++;
				}
				if (!act.contains("Note")) {
					contador++;
					errores.add("Journal");
				}
				if (contador!=0)
				{
					errores.add(0, ID);
					faltantes.add(errores.toString());
				}
			}
		}
	
	}

	private void contarErrores() throws IOException {

		int lineaActual = 1;
		String actual = br.readLine();
		System.out.println(actual);
		while (actual != null) {
			
			lineaActual++;
			if (actual.contains("&") || actual.contains("'") || actual.contains("#") || actual.contains("\\")
					|| actual.contains("+")) {
				totalErrores++;
				arrayErroresLinea.add(lineaActual);
				actual = br.readLine();
			} else {
				actual = br.readLine();
			}
		}
		
	}
	
	public int darTotalErrores(){
		return totalErrores;
	}
	
	public ArrayList<Integer> darArrayLineasError(){
		return arrayErroresLinea;
	}
	
	public ArrayList<String> darFaltantes(){
		return faltantes;
	}

}
