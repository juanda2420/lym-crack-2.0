package Excepcions_ibtex_file;

import java.io.File;
import java.io.PrintWriter;
import java.util.Date;

public class GeneralException extends Exception {
	/**
	 * 
	 */
	private static final long serialVersionUID = -1034140082578813595L;

	private  String mensaje;

	private String ARCHIVO_LOG_ERRORES = "data/log/error.log";

	public GeneralException (String pMensaje) {
		super(pMensaje);
		escribirlog(pMensaje);
		this.mensaje = pMensaje;
	}

	public String darMensajeError() {
		return mensaje;
	}

	protected String darNombreError() {
		
		return this.darMensajeError();
	}

	public void escribirlog(String pMensaje) {

		try {
			PrintWriter PW = new PrintWriter(new File(ARCHIVO_LOG_ERRORES));
			String ms = new Date().toString() + " COT 2018 " + this.darNombreError() +" - " + this.getMessage();
			PW.println(ms);
			PW.flush();

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
