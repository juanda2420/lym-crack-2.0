package Excepcions_ibtex_file;


public class PersistenciaException extends GeneralException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4406748149892686726L;

	private String ruta;

	public PersistenciaException (String pMensaje, String ruta) {
		super("Ha ocurrido un error de entrada/salida:  " +  ruta + " Error: " + pMensaje);
		this.ruta = ruta;
	}

	public String darRuta() {
		return ruta;
	}

	protected String darNombreError() {
		return "error de persistencia";
	}

}

