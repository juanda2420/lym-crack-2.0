package interfaz_bibtex_file;

import java.awt.Color;
import java.awt.Dimension;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.LineBorder;

public class PanelImagenProyecto extends JPanel {
	    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	

		public PanelImagenProyecto( )
	    {
	    	JLabel imagen = new JLabel( );
	        ImageIcon icono = new ImageIcon( "data/bibtex.png" );
	        
	        
	        // La agrega a la etiqueta
	        imagen = new JLabel( "" );
	        imagen.setIcon( icono );
	        imagen.setPreferredSize(new Dimension(1000, 100));
	        
	        add( imagen );

	        setBackground( Color.WHITE );
	        setBorder( new LineBorder( Color.BLACK ) );
	    }
	}
