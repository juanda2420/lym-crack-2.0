package interfaz_bibtex_file;

import java.awt.BorderLayout;
import java.awt.Color;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;

import javax.swing.ImageIcon;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.UIManager;

import Excepcions_ibtex_file.PersistenciaException;
import mundo_bibtex_file.bibtex_file;


public class Ibibtex_file extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	// -------------------------------------------------
	
	
	private bibtex_file mundo;

	private PanelImagenProyecto panelIMagen;


	private PanelTexto panelTexto;

	private PanelRutaArchivo panelRutaArchivo;
	
	private File archivoSeleccionado;
	
	private int totalErrores;
	
	private ArrayList<Integer> lineasError;
	
	private ArrayList<String> faltantesMundo;

	public Ibibtex_file() throws FileNotFoundException {

		setTitle("Ibibtex_File");
		setSize(1000, 1000);
		setBackground(Color.WHITE);
		setDefaultCloseOperation(javax.swing.JFrame.EXIT_ON_CLOSE);

		//mundo = new bibtex_file();
		

		setLayout(new BorderLayout());

		panelIMagen = new PanelImagenProyecto();
		add(panelIMagen, BorderLayout.NORTH);


		panelTexto = new PanelTexto(this,archivoSeleccionado,totalErrores, lineasError, faltantesMundo);
		add(panelTexto, BorderLayout.CENTER);

		panelRutaArchivo = new PanelRutaArchivo(this);
		add(panelRutaArchivo, BorderLayout.SOUTH);
		
		setLocationRelativeTo(null);
		setResizable(false);

	}


	/**
	 * M�todo que carga el Karaoke desde un archivo serializado.
	 * @throws IOException 
	 */
	public void abrirCarpetaArchivos() throws Excepcions_ibtex_file.PersistenciaException, IOException {

		String ruta = "";

		JFileChooser jf = new JFileChooser("data/");

		if (jf.showSaveDialog(null) == jf.APPROVE_OPTION) {

			ruta = jf.getSelectedFile().getAbsolutePath();
			
			mundo = new bibtex_file(ruta);
			archivoSeleccionado = new File(ruta);
			totalErrores = mundo.darTotalErrores();
			lineasError = mundo.darArrayLineasError();
			faltantesMundo = mundo.darFaltantes();
			panelTexto.actualizar(archivoSeleccionado, totalErrores, lineasError, faltantesMundo);
			
//
//				karaoke = Serializador.cargar(ruta, karaoke);

		}
		else {
			JOptionPane.showMessageDialog(this, "Ha ocurrido un error al cargar el archivo", "Error", JOptionPane.ERROR_MESSAGE);
		}
		
	}
	

	

	/**
	 * Ejecuta la aplicaci�n.
	 * 
	 * @param pArgs Par�metros de la ejecuci�n. No son necesarios.
	 */
	public static void main(String[] pArgs) {
		try {
			// Unifica la interfaz para Mac y para Windows.
			UIManager.setLookAndFeel(UIManager.getCrossPlatformLookAndFeelClassName());

			Ibibtex_file interfaz = new Ibibtex_file();
			interfaz.setVisible(true);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
