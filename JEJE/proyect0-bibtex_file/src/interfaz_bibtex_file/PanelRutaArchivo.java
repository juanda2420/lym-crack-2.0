package interfaz_bibtex_file;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileNotFoundException;
import java.io.IOException;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.LineBorder;

import Excepcions_ibtex_file.PersistenciaException;

public class PanelRutaArchivo extends JPanel implements ActionListener  {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private JButton boton;
	
	private JTextField txtInformacion;
	
	private Ibibtex_file interfaz;
	
	
	private static String RUTA_ARCHIVO = "ruta Archivo";
	
	
	public PanelRutaArchivo(Ibibtex_file pInterfaz) {
		
	
	      setBackground( Color.WHITE );
	      setBorder( new LineBorder( Color.BLACK ) );
	      setPreferredSize(new Dimension(1000, 40));
	      
		txtInformacion = new JTextField();
		txtInformacion.setText("Por favor, seleccione el archivo deseado a revisar");
		txtInformacion.setEditable(false);
		
		add(txtInformacion);
	      
		interfaz = pInterfaz;
		boton = new JButton();
		boton.setText("Ruta");
		boton.setBackground(Color.GRAY);
		boton.addActionListener(this);
		boton.setActionCommand(RUTA_ARCHIVO);
		
		 add( boton);

	
	}


	@Override
	public void actionPerformed(ActionEvent e) {
		String x = e.getActionCommand();
		
		if (x.equals(RUTA_ARCHIVO)){
			try {
				interfaz.abrirCarpetaArchivos();
			} catch (PersistenciaException | FileNotFoundException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
	}
}
