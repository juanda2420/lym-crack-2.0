package interfaz_bibtex_file;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Scanner;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.border.TitledBorder;
import Excepcions_ibtex_file.PersistenciaException;

public class PanelTexto extends JPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private JTextArea txtInfoBibtex;

	private JTextArea txtErrores;

	String textoBibtext;

	String textoErrores;

	Ibibtex_file principal;

	private int totalErrores;
	
	private ArrayList<Integer> lineasError;
	
	private ArrayList<String> faltantesMundo;
	

	public PanelTexto( Ibibtex_file pPrincipal,File archivo, int pCantidadErrores, ArrayList<Integer> pArregloLineasError,
			ArrayList<String> Pfaltantes) throws FileNotFoundException {

		setBorder(new CompoundBorder(new TitledBorder("Datos :"), new EmptyBorder(0, 0, 0, 5)));
		setPreferredSize(new Dimension(250, 0));
		setLayout(new BorderLayout());

		principal = pPrincipal;

		JPanel TextoOriginal = new JPanel();
		TextoOriginal.setLayout(new BorderLayout());
		TextoOriginal.setPreferredSize(new Dimension(550, 450));

		JPanel TextoErrores = new JPanel();
		TextoErrores.setLayout(new BorderLayout());
		TextoErrores.setPreferredSize(new Dimension(400, 450));

		TextoOriginal.add(new JLabel("Informacion Original Presentada: "), BorderLayout.NORTH);
		TextoErrores.add(new JLabel("Informacion Errores Presentados: "), BorderLayout.NORTH);

		txtInfoBibtex = new JTextArea();
		txtInfoBibtex.setEditable(false);
		JScrollPane scrollInfoText = new JScrollPane(txtInfoBibtex);
		scrollInfoText.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
		scrollInfoText.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		TextoOriginal.add(scrollInfoText, BorderLayout.CENTER);

		txtErrores = new JTextArea();
		txtErrores.setEditable(false);
		JScrollPane scrollInfoErrores = new JScrollPane(txtErrores);
		scrollInfoErrores.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
		scrollInfoErrores.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		TextoErrores.add(scrollInfoErrores, BorderLayout.CENTER);
		
		
		
		actualizar(archivo,pCantidadErrores, pArregloLineasError, Pfaltantes);

		add(TextoOriginal, BorderLayout.EAST);
		add(TextoErrores, BorderLayout.WEST);

	}

	public void actualizar(File archivo, int pCantidadErrores, ArrayList<Integer> pArregloLineasError,
			ArrayList<String> Pfaltantes) throws FileNotFoundException {
		
		if (archivo != null) {
			informacionErrores(pCantidadErrores, pArregloLineasError, Pfaltantes);
			Scanner scanner = new Scanner(archivo);
			textoBibtext = scanner.useDelimiter("\\A").next();
			scanner.close();
			txtInfoBibtex.setText(textoBibtext);
			txtInfoBibtex.setCaretPosition(0);
			txtErrores.setText(textoErrores);
			txtErrores.setCaretPosition(0);

		} 
		else {
			txtInfoBibtex.setText(textoBibtext);
		}

	}

	public void informacionErrores(int pCantidadErrores, ArrayList<Integer> pArregloLineasError,
			ArrayList<String> Pfaltantes) {

		textoErrores = pCantidadErrores + "";
		
//		
//		ArrayList<String> lineasErrorConEspacio = new ArrayList<String>();
//
//		ArrayList<String> lineasFaltantesConEspacio = new ArrayList<String>();
//
//		
//		for (int i = 0; i < pArregloLineasError.size(); i++) {
//			lineasErrorConEspacio.add(pArregloLineasError.get(i) + "\n");
//		}
//		for (int i = 0; i < Pfaltantes.size(); i++) {
//			String actual = Pfaltantes.get(i);
//
//			String[] arregloActual = actual.split(",");
//
//			ArrayList<String> datosActual = new ArrayList<String>();
//
//			for (int a = 1; a < arregloActual.length; a++) {
//				datosActual.add(arregloActual[a]);
//			}
//
//			for (int j = 0; j < Pfaltantes.size(); j++) {
//				lineasFaltantesConEspacio.add("PARA EL TEXTO CON IDENTIFICADOR : " + arregloActual[0]
//						+ " LE HICIERON FALTA LOS SIGUIENTES CAMPOS OBLIGATORIOS :  " + datosActual.toString() + "\n");
//			}
//
//		}
//
//		textoErrores = "LA CANTIDAD DE ERRORES PRESENTADOS FUERON : " + pCantidadErrores + "\n"
//				+ "LAS LINEAS DONDE SE PRESENTARON LOS ERRORES FUERON :" + "\n" + lineasErrorConEspacio.toString()
//				+ "\n" + "LOS DATOS FALTANTES FUERON :" + "\n" + lineasFaltantesConEspacio.toString();

	}

}
